package model;

public class SavingAccount extends Account implements java.io.Serializable{

	//private static final long serialVersionUID = -7114794952637741810L;

	public SavingAccount(Person mainHolder, String accName) {
		super(mainHolder, accName);
	}
	
	// can only deposit when it's empty
	public void deposit(int value){
		if(funds == 0)
			funds += (int)(value * 1.1); // calculates interest as well
	}
	
	// withdrawal sets the funds to 0, making a deposit possible again
	public int withdraw(int value){
		int aux = funds;
		funds = 0;
		return aux;
	}
}
