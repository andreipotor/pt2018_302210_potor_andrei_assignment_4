package model;
import java.util.ArrayList;
import java.util.Hashtable;


public class Bank implements BankProc, java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private Hashtable<Person, ArrayList<Account>> h;
	
	// Constructor
	public Bank(){
		h = new Hashtable<Person, ArrayList<Account>>();
	}
	
	@Override
	public void addPerson(Person p){
		h.put(p, new ArrayList<Account>());
	}
	
	@Override
	public void removePerson(Person p) {
		h.remove(p);
	}
	
	@Override
	public void addAccount(Person p, Account newAccount){
		if(h.get(p) == null){
			ArrayList<Account> temp = new ArrayList<Account>();
			temp.add(newAccount);
			h.put(p, temp);
		}
		else{
			h.get(p).add(newAccount);
		}
	}
	
	@Override
	public void removeAccount(Person p, Account acc){
		if(h.get(p) != null){
			h.get(p).remove(acc); // i implemented the equals method in class Account
		}
	}

	@Override
	public void deposit(Person p, Account acc, int ammount) {
		h.get(p).get(   h.get(p).indexOf(acc)   ).deposit(ammount);
	}

	@Override
	public void withdraw(Person p, Account acc, int ammount) {
		h.get(p).get(   h.get(p).indexOf(acc)   ).withdraw(ammount);
	}
	
	public String[][] getPersonStrings(){
		int count = 0;
		String[][] res = new String[h.keySet().size()][2];
		for(Person p: h.keySet()){
			res[count][0] = p.getLastName();
			res[count++][1] = p.getFirstName();
		}
		return res;
	}
	

	
	public String[][] getAccountStrings(Person p){
		int count = 0;
		String[][] res = new String[h.get(p).size()][3];
		for(Account acc: h.get(p)){
			res[count][0] = acc.getPerson().getLastName() + " " + acc.getPerson().getFirstName();
			res[count][1] = acc.getName();
			res[count++][2] = String.valueOf(acc.getFunds());
		}
		return res;
	}
	

	
}
