package model;

public interface BankProc {
	public void addPerson(Person p);
	public void removePerson(Person p);
	
	public void addAccount(Person p, Account acc);
	public void removeAccount(Person p, Account acc);
	
	public void deposit(Person p, Account acc, int ammount);
	public void withdraw(Person p, Account acc, int ammount);

}
