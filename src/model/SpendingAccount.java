package model;

public class SpendingAccount extends Account implements java.io.Serializable{
	
	//private static final long serialVersionUID = -1461847656906124430L;

	public SpendingAccount(Person mainHolder, String accName) {
		super(mainHolder, accName);
	}
	
	public void deposit(int value){
		funds += value;
	}
	
	public int withdraw(int value){
		if(funds >= value){
			funds -= value;
			return value;
		}
		return 0;
	}
}
