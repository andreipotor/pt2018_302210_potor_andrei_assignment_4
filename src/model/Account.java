package model;

public class Account implements java.io.Serializable{

	//private static final long serialVersionUID = 6878117410592687400L;
	
	private Person mainHolder;
	private String name = "";
	protected int funds = 0;
	
	public Account(Person mainHolder, String accName){
		this.mainHolder = mainHolder;
		this.name = accName;
	}
	
	public Account(Person mainHolder, String accName, int funds){
		this.mainHolder = mainHolder;
		this.name = accName;
		this.funds = funds;
	}
	
	public void deposit(int value){
		// empty because we will always use one of the subclasses
	}
	
	public int withdraw(int value){
		// empty because we will always use one of the subclasses
		return -1;
	}
	
	public Person getPerson(){
		return mainHolder;
	}
	
	public String getName(){
		return name;
	}
	
	public int getFunds(){
		return funds;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		
		Account temp = (Account)obj;
		if((!mainHolder.equals(temp.getPerson()))||(name != temp.getName()))
			return false;
		
		return true;
	}
}
