package model;

public class Person implements java.io.Serializable{
	
	//private static final long serialVersionUID = 2041138520032057780L;
	
	private String firstName = "";
	private String lastName = "";
	
	public Person(String lastName, String firstName){
		this.firstName = capitalize(firstName);
		this.lastName = capitalize(lastName);
	}
	
	private String capitalize(String s){
		String res = "";
		String aux;
		
		if((s != null)&&(s.length() > 0)){
			res += s.charAt(0);
			res.toUpperCase();
			
			aux = s.substring(1);
			aux.toLowerCase();
			
			res += aux;
		}
		
		return res;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String toString(){
		return lastName + " " + firstName;
	}
	
	@Override
	public int hashCode(){
		if((lastName.length() > 0) && (firstName.length() > 0))
			return (lastName.charAt(0) - 'A') * 10 + (firstName.charAt(1) - 'A');
		return -1;
	}
	
	@Override
	public boolean equals(Object o){
		// null
		if(o == null)
			return false;
		
		// different class
		if(o.getClass() != getClass())
			return false;
		
		Person p = (Person)o;
		
		// check first name
		if(!p.getFirstName().equals(firstName))
			return false;
		
		// check last name
		if(!p.getLastName().equals(lastName))
			return false;
		
		// <else> it's equal
		return true;
	}
}
