package controller;

	import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.JButton;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import model.Account;
import model.Bank;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;
import view.GUI;

public final class Controller {
	private Bank b;
	private ActionListener l;
	private ListSelectionListener plsl; // persons
	private ListSelectionListener alsl; // accounts
	private GUI gui;
	private Person selectedPerson;
	private Account selectedAccount;

	
	public Controller(){
		b = new Bank();
		gui = new GUI();
		
		inputBank();
		
		initActionListener();
		initPersonSelectionListener();
		initAccountSelectionListener();
		
		updatePersons();
		
		gui.setButtonsActionListener(l);
		gui.setPersonSelectionListener(plsl);
		gui.setAccountSelectionListener(alsl);
		
		gui.setVisible(true);
	}
	
	private void outputBank(){
		 try {
	         FileOutputStream fileOut = new FileOutputStream("bank.ser");
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(b);
	         out.close();
	         fileOut.close();
	         gui.setInfoLabel("Saved the bank in file \"bank.ser\"");
	      } catch (IOException i) {
	         i.printStackTrace();
	      }
	}
	
	private void inputBank(){
		b = null;
		try {
	         FileInputStream fileIn = new FileInputStream("bank.ser");
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         b = (Bank) in.readObject();
	         in.close();
	         fileIn.close();
	         System.out.println("Successfully loaded data from file \"bank.ser\"");
	         gui.setInfoLabel("Successfully loaded data from file \"bank.ser\"");
	      } catch (IOException i) {
		     gui.setInfoLabel("Could not import bank from file \"bank.ser\"");
	         i.printStackTrace();
	         return;
	      } catch (ClassNotFoundException c) {
	         System.out.println("Bank class not found");
	         gui.setInfoLabel("Bank class not found");
	         c.printStackTrace();
	         return;
	      }
	}
	
	// added so the initActionListener method would not get too big
	private void addAccount(){
		if(selectedPerson != null)
    		if(gui.savingAccSelected()){
    			b.addAccount(selectedPerson, 
    					new SavingAccount(selectedPerson, getTextBox1() + " (saving)"));
    		}
    		else{
    			b.addAccount(selectedPerson, 
    					new SpendingAccount(selectedPerson, getTextBox1() + " (spending)"));
    		}
	}
	
	private void initActionListener(){
		l = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(e.getSource().getClass() == JButton.class){
					switch (((JButton)e.getSource()).getText()) {
		            case "Add Person":
		            	b.addPerson(new Person(getTextBox1(), getTextBox2()));
		            	updatePersons();
		                break;
		            case "<html>Remove<br>Person":
		            	if(selectedPerson != null){
		            		b.removePerson(selectedPerson);
		            		updatePersons();
		            	}
		                break;
		            case "<html>Add<br>Account":
		            	addAccount();
		            	updateAccounts();
		                break;
		            case "<html>Remove<br>Account":
		            	if((selectedPerson != null)&&(selectedAccount != null)){
		            		b.removeAccount(selectedPerson, selectedAccount);
		            		updateAccounts();
		            	}
		                break;
		            case "Deposit":  
		            	if((selectedPerson != null)&&(selectedAccount != null)){
		            		b.deposit(selectedPerson, selectedAccount, Integer.parseInt(getTextBox1()));
		            		updateAccounts();
		            	}
		                break;
		            case "Withdraw":  
		            	if((selectedPerson != null)&&(selectedAccount != null)){
		            		b.withdraw(selectedPerson, selectedAccount, Integer.parseInt(getTextBox1()));
		            		updateAccounts();
		            	}
		                break;
		            case "Save Data":  
		            	outputBank();
		                break;
					}
				}
			}
		};
	}
	
	private void initPersonSelectionListener(){
		plsl = new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if((!e.getValueIsAdjusting())&&(gui.getSelectedPersonRow() >= 0)){ // in order not to be called twice
					String[] aux = gui.getSelectedPersonData();
					selectedPerson = new Person(aux[0], aux[1]);
					updateAccounts();
				}
			}
		};
	}
	
	private void initAccountSelectionListener(){
		alsl = new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if((!e.getValueIsAdjusting())&&(gui.getSelectedAccountRow() >= 0)){ // in order not to be called twice
					String[] aux = gui.getSelectedAccountData();
					String[] person = aux[0].split(" ");
					selectedAccount = new Account(new Person(person[0], person[1]), aux[1], (int)Integer.valueOf(aux[2]));
				}
			}
		};
	}
	
	private String getTextBox1(){
		return gui.getTextBox1();
	}
	
	private String getTextBox2(){
		return gui.getTextBox2();
	}
	
	private void updatePersons(){
		gui.updatePersons(b.getPersonStrings());
		gui.revalidate();
		gui.repaint();
	}
	
	private void updateAccounts(){
		gui.updateAccounts(b.getAccountStrings(selectedPerson));
		gui.revalidate();
		gui.repaint();
	}
	
}
