package view;

import java.awt.event.ActionListener;

import javax.swing.event.ListSelectionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

public class GUI extends JFrame{
	private static final long serialVersionUID = 1L;
	
	private JButton addPersonButton, removePersonButton, addAccountButton, removeAccountButton, 
					depositButton, withdrawButton, saveDataButton;
	private JTextField tb1, tb2;
	private JLabel infoLabel, label1, label2;
	private JRadioButton saving, spending;
	private JTable persons, accounts;
	private JScrollPane scrollPane1, scrollPane2;
	private ButtonGroup group;
	
	public GUI(){
		this.setTitle("Potor Andrei - Tema 4 - Banca");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(925, 500);
		this.setLayout(null);
		group = new ButtonGroup();
		
		persons = new JTable();
		accounts = new JTable();
		
		scrollPane1 = new JScrollPane();
		scrollPane1.setSize(430, 300);
		scrollPane1.setLocation(10, 30);
		this.add(scrollPane1);
		
		scrollPane2 = new JScrollPane();
		scrollPane2.setSize(430, 300);
		scrollPane2.setLocation(450, 30);
		this.add(scrollPane2);
		
		tb1 = new JTextField();
		tb1.setSize(430, 25);
		tb1.setLocation(10, 365);
		this.add(tb1);
		
		label1 = new JLabel("Person last name / Deposit ammount / Withdraw ammount / Account name");
		label1.setSize(430, 20);
		label1.setLocation(11, 345);
		this.add(label1);
		
		tb2 = new JTextField();
		tb2.setSize(430, 25);
		tb2.setLocation(450, 365);
		this.add(tb2);
		
		label2 = new JLabel("Person first name");
		label2.setSize(430, 20);
		label2.setLocation(450, 345);
		this.add(label2);
		
		saving = new JRadioButton("Saving Account");
		saving.setLocation(775, 405);
		saving.setSize(200,20);
		saving.setSelected(true);
		group.add(saving);
		this.add(saving);
		
		spending = new JRadioButton("Spending Account");
		spending.setLocation(775, 425);
		spending.setSize(200,20);
		group.add(spending);
		this.add(spending);
		
		infoLabel = new JLabel("<Info>");
		infoLabel.setSize(1000, 20);
		infoLabel.setLocation(7,0);
		this.add(infoLabel);
		
		addPersonButton = new JButton();
		addPersonButton.setSize(100, 50);
		addPersonButton.setLocation(10, 400);
		addPersonButton.setText("Add Person");
		this.add(addPersonButton);
		
		removePersonButton = new JButton();
		removePersonButton.setSize(100, 50);
		removePersonButton.setLocation(120, 400);
		removePersonButton.setText("<html>Remove<br>Person");
		this.add(removePersonButton);
		
		addAccountButton = new JButton();
		addAccountButton.setSize(100, 50);
		addAccountButton.setLocation(230, 400);
		addAccountButton.setText("<html>Add<br>Account");
		this.add(addAccountButton);
		
		removeAccountButton = new JButton();
		removeAccountButton.setSize(100, 50);
		removeAccountButton.setLocation(340, 400);
		removeAccountButton.setText("<html>Remove<br>Account");
		this.add(removeAccountButton);
		
		depositButton = new JButton();
		depositButton.setSize(100, 50);
		depositButton.setLocation(450, 400);
		depositButton.setText("Deposit");
		this.add(depositButton);
		
		withdrawButton = new JButton();
		withdrawButton.setSize(100, 50);
		withdrawButton.setLocation(560, 400);
		withdrawButton.setText("Withdraw");
		this.add(withdrawButton);
		
		saveDataButton = new JButton();
		saveDataButton.setSize(100, 50);
		saveDataButton.setLocation(670, 400);
		saveDataButton.setText("Save Data");
		this.add(saveDataButton);
	}
	
	public void setButtonsActionListener(ActionListener l){
		addPersonButton.addActionListener(l);
		removePersonButton.addActionListener(l);
		addAccountButton.addActionListener(l);
		removeAccountButton.addActionListener(l);
		depositButton.addActionListener(l);
		withdrawButton.addActionListener(l);
		saveDataButton.addActionListener(l);
	}
	
	public void setPersonSelectionListener(ListSelectionListener lsl){
		persons.getSelectionModel().addListSelectionListener(lsl);
	}
	
	public void setAccountSelectionListener(ListSelectionListener lsl){
		accounts.getSelectionModel().addListSelectionListener(lsl);
	}
	
	public void setInfoLabel(String s){
		infoLabel.setText(s);
	}
	
	public String getTextBox1(){
		return tb1.getText();
	}
	
	public String getTextBox2(){
		return tb2.getText();
	}
	
	public boolean savingAccSelected(){
		return saving.isSelected();
	}
	
	public int getSelectedPersonRow(){
		return persons.getSelectedRow();
	}
	
	public int getSelectedAccountRow(){
		return accounts.getSelectedRow();
	}
	
	public String[] getSelectedPersonData(){
		String[] res = new String[2];
		res[0] = (String)persons.getValueAt(persons.getSelectedRow(), 0);
		res[1] = (String)persons.getValueAt(persons.getSelectedRow(), 1);
		return res;
	}
	
	public String[] getSelectedAccountData(){
		String[] res = null;
		try {
			res = new String[3];
			res[0] = (String)accounts.getValueAt(accounts.getSelectedRow(), 0);
			res[1] = (String)accounts.getValueAt(accounts.getSelectedRow(), 1);
			res[2] = String.valueOf(accounts.getValueAt(accounts.getSelectedRow(), 2));
		} catch (Exception e) {
			
		}
		return res;
	}
	
	public void updatePersons(String[][] data){
		String[] columnNames = {"Last Name", "First Name"};	
		JTable aux = new JTable(data,columnNames); // have to use this in order not to lose the selection listener
		persons.removeAll();
		persons.setModel(aux.getModel());
		persons.setFillsViewportHeight(true);
		scrollPane1.setViewportView(persons);
	}
	
	public void updateAccounts(String[][] data){
		String[] columnNames = {"Main Holder", "Account Name", "Funds"};	
		JTable aux = new JTable(data,columnNames); // have to use this in order not to lose the selection listener
		accounts.removeAll();
		accounts.setModel(aux.getModel());
		accounts.setFillsViewportHeight(true);
		scrollPane2.setViewportView(accounts);
	}
}







